package substitution

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

var unsafeSymbols = []string{".", "\\", "+", "*", "?", "[", "^", "]", "$", "(", ")", "{", "}", "=", "!", "<", ">", "|", ":", "-"}

// MacroSymbol is the symbol to be used around macros, as to mark them as words that should be substituted.
var MacroSymbol = "%"

// Do replaces all macros in input with their corresponding substitutions by key. Macros in input should be surrounded by MacroSymbol (default "%"). An error is returned if input contains any macro for which there is no substitution, and/or if substitutions contains a key for which there is no macro in input.
func Do(input string, substitutions map[string]string) (output string, err error) {
	regSymbol := MacroSymbol
	if !isSafe() {
		regSymbol = `\` + regSymbol
	}
	exp, err := regexp.Compile(regSymbol + `[^ %]+` + regSymbol)
	if err != nil {
		return "", fmt.Errorf("cannot use MacroSymbol %v: %w", MacroSymbol, err)
	}
	found := make(map[string]string)
	output = exp.ReplaceAllStringFunc(input, func(macro string) string {
		macro = strings.TrimPrefix(macro, MacroSymbol)
		macro = strings.TrimSuffix(macro, MacroSymbol)
		sub, ok := substitutions[macro]
		if !ok {
			err = fmt.Errorf("substitutions contains no element for macro %v", macro)
			return ""
		}
		found[macro] = sub
		return sub
	})
	for key := range substitutions {
		if _, ok := found[key]; !ok {
			err = errors.New("substitutions contains an element " + key + " for which there is no macro in input")
			break
		}
	}
	return
}

func isSafe() bool {
	for _, symbol := range unsafeSymbols {
		if MacroSymbol == symbol {
			return false
		}
	}
	return true
}
